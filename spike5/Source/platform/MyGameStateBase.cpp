// Fill out your copyright notice in the Description page of Project Settings.

#include "platform.h"
#include "MyGameStateBase.h"




int32 AMyGameStateBase::SetScore(int32 value)
{
	currentScore += value;

	return currentScore;
}

int32 AMyGameStateBase::GetScore()
{
	return currentScore;
}

float AMyGameStateBase::SetCurrentHealth(float Damage)
{
	if (CurrentHealth > 0)
	{
		CurrentHealth -= Damage;
	}
	return CurrentHealth;
}

float AMyGameStateBase::Sethealth(float baseHealth, float totalBaseHealth)
{

	health = baseHealth / totalBaseHealth;

	return health;
}

void AMyGameStateBase::BeginPlay()
{

	TotalHealth = 100.f;
	CurrentHealth = TotalHealth;

	health = CurrentHealth / TotalHealth;

}



