// Fill out your copyright notice in the Description page of Project Settings.

#include "platform.h"
#include "MainPlayerController.h"


void AMainPlayerController::BeginPlay()
{
	Super::BeginPlay();

	auto ControlledCharacter = AMainPlayerController::GetPlatformCharacter();
	if (!ControlledCharacter)
	{
		UE_LOG(LogTemp, Warning, TEXT("Not Possessing Player"));
		ControlledCharacter = AMainPlayerController::GetPlatformCharacter();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Possessing Player"));
	}
	
}

void AMainPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	if (this != NULL)
	{
		InputComponent->BindAxis("MoveForward", this, &AMainPlayerController::MoveCharacter);
		InputComponent->BindAction("Jump", IE_Pressed, this, &AMainPlayerController::OnStartJump);
		InputComponent->BindAction("Jump", IE_Released, this, &AMainPlayerController::OnStopJump);
	}

}

void AMainPlayerController::MoveCharacter(float Value)
{
	if (ControlledCharacter)
	{
		AMainPlayerController::GetPlatformCharacter()->MoveForward(Value);
	}
}

void AMainPlayerController::OnStartJump()
{
	if (ControlledCharacter)
	{
		AMainPlayerController::GetPlatformCharacter()->OnStartJump();
	}
}

void AMainPlayerController::OnStopJump()
{
	if (ControlledCharacter)
	{
		AMainPlayerController::GetPlatformCharacter()->OnStopJump();
	}
}

void AMainPlayerController::Tick(float DeltaTime)
{
	ControlledCharacter = AMainPlayerController::GetPlatformCharacter();
}


APlatformCharacter * AMainPlayerController::GetPlatformCharacter() const
{
	return Cast<APlatformCharacter>(GetControlledPawn());
}