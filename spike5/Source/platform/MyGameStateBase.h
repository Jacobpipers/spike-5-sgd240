// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameStateBase.h"
#include "MyGameStateBase.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class PLATFORM_API AMyGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "Score")
	int32 GetScore();
	UFUNCTION(BlueprintCallable, Category = "Score")
	int32 SetScore(int32 value);

	UFUNCTION(BlueprintCallable, Category = "Damage")
	float SetCurrentHealth(float damage);

	UFUNCTION(BlueprintCallable, Category = "Damage")
	float Sethealth(float baseHealth, float totalBaseHealth);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Values")
	float health;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Damage")
	float TotalHealth;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Damage")
	float CurrentHealth;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Values")
	int32 currentScore;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
};