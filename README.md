# Spike Report

## SPIKE 5 - Unreal Engine UI (UMG)

### Introduction

We’ve come a long way already, with a little game prototype that handles most of the Unreal Gameplay Framework, and now we want to put in some polish into our game. 
Any gameplay-complete prototype needs a few things to make the User eXperience complete – and we’re going to start by learning how to make a HUD (Heads Up Display) and a Menu


### Goals

Building on Core Spike 4 (Unreal Engine Gameplay Framework), add:

* A Heads-Up Display which displays some values of the Pawn which we are possessing. You can do this in one of several ways:
  	* [Preferred] In your custom PlayerController, create your Widget and attach it
  	* [Rudimentary] In the Level Blueprint, create/attach the Widget to Player Controller 0
	* [Depreciated] Add a custom HUD class to your custom GameMode
  
* A Main Menu, in a separate scene, which has 3 buttons:
 	* Start a New Game
 	* Show some Info (Gameplay controls? Credits?)
 	* Or Quit the game
* A Pause Menu, during which
	* The game pauses, and the mouse is shown (if hidden)
 	* The player can Return to the Game (re-hides the mouse)
 	* The player can Go to the Menu (change scene)


### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* [UMG Quick Start](https://docs.unrealengine.com/latest/INT/Engine/UMG/QuickStart/2/index.html)
* [Main Menu](https://docs.unrealengine.com/latest/INT/Engine/UMG/QuickStart/3/index.html)
* [Scripting Main Menu](https://docs.unrealengine.com/latest/INT/Engine/UMG/QuickStart/4/index.html)
* [UMG UI Designer Documentation](https://docs.unrealengine.com/latest/INT/Engine/UMG/index.html)
* Visual Studio
* Unreal Engine


### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

* Start from where we finished in spike 4
* Create a Widget and name it HUD for the Heads-Up Display:
	* Follow the Quickstart guide: [UMG Quickstart](https://docs.unrealengine.com/latest/INT/Engine/UMG/QuickStart/2/index.html) 
	* Instead of Player Character seen in the tutorial Use get GameState as seen in picture below.
	
		 ![](RetrieveGameState.png)
	
	* Create new [Binding](https://docs.unrealengine.com/latest/INT/Engine/UMG/UserGuide/PropertyBinding/index.html)

		 ![](Bindings.png)

* Create a new blank level called Main Menu 
	* Follow this [Main Menu](https://docs.unrealengine.com/latest/INT/Engine/UMG/QuickStart/3/index.html) Guide
	* [Scripting Main Menu](https://docs.unrealengine.com/latest/INT/Engine/UMG/QuickStart/4/index.html)

* Follow this guide: [Creating / Scripting an In-Game Pause Menu](https://docs.unrealengine.com/latest/INT/Engine/UMG/QuickStart/5/index.html) to create a pause menu containing a resume and quit buttons. Making sure to show cursor when game is paused. Ussing the Game Level blueprint to check if game is a good place to check if game is paused and showing mouse if game is paused.


### What we found out

UMG is the prefered way to create Menus and User Interfaces in the Unreal engine. 



### [Optional] Open Issues/Risks

List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.